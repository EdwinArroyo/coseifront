module.exports = {
  publicPath: '/coseipage/',
  pwa: {
    name: 'app',
    themeColor: '#C62828',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'red',

    // configure the workbox plugin
    //workboxPluginMode: 'InjectManifest',
    //workboxOptions: {
      // swSrc is required in InjectManifest mode.
      //swSrc: 'dev/sw.js',
      // ...other Workbox options...
    //}
  }
}